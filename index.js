let stocks = {
    fruits: ["strawberry", "grapes", "banana", "apple"],
    liquid: ["water", "ice"],
    holder: ["cone", "cup", "stick"],
    toppings: ["chocolate", "peanuts"]
}

let is_shop_open = true;

const time = (ms) => {
    return new Promise((resolve, reject) => {
        if ( is_shop_open ) {
            setTimeout(resolve, ms)
        } else {
            reject(console.log("Shop is closed"));
        }
    });
}

const kitchen = async () => {
    try {
        await time(2000)
        console.log(`${stocks.fruits[0]} was selected`);

        await time(0000)
        console.log("Starting the production");

        await time(2000)
        console.log("Cut the fruit");

        await time(1000)
        console.log(`${stocks.liquid[0]} and ${stocks.liquid[1]} was added`);

        await time(1000)
        console.log("start the machine")

        await time(2000)
        console.log(`Ice cream placed on ${stocks.holder[0]}`)

        await time(3000)
        console.log(`${stocks.toppings[0]}`)

        await time(1000)
        console.log(" serve ice cream");

    } catch (error) {
        console.log("Customer left", error)
    }
    finally {
        console.log("day ended, shop is closed")
    }
}

kitchen();